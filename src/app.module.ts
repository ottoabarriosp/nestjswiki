import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './products/middlewares/logger.middleware';
import { logger } from './products/middlewares/loggerFunction.middleware';
import { ProductsModule } from './products/products.module';
import * as cors from 'cors';
import * as helmet from 'helmet';
/*
modulo raiz de la app

Un modulo es una clase con el decorador @Module.
El mismo, proporciona metadatos a la clase que Nest
utiliza para organizar la estructura de la app.

Cada aplicacion tiene al menos un modulo raiz, aunque
la mayoria de las aplicaciones pueden emplear multiples modulos,
encapsulando de esta forma un conjunto de capacidades relacionadas.

ver archivo /products/products.module.ts
*/
@Module({
  imports: [ProductsModule],
  /*
  la lista de módulos importados que exportan los proveedores 
  que se requieren en este módulo
  */
  controllers: [AppController],
  /* 
  todos los controladores deben pertenecer a un modulo para que Nest
  pueda saber que exite. de lo contrario no creara una instancia de la clase.
  asi que siempre al crear un controller nuevo, debemos agregarlo a un modulo.
  */
  providers: [AppService],
  /*
  Los providers o proveedores son fundamentales en Nest.
  muchas de las clases basicas pueden tratarse como proveedor
  (services, repositories, factories, helpers etc)
  la idea principal de un provider es inyectar dependencias.

  esto significa que los objetos pueden crear varias relaciones entre si, y
  la funcion de conectar instancias de objetos se delega a Nest.

  un provider es simplemente una clase con el decorador @Injectable()
  ver archivo app.service.ts
  */
  exports: [],
  /*
  el subconjunto de los providersque proporciona este módulo 
  y debería estar disponible en otros módulos que importan este módulo
  */
})

/*
El módulo encapsula proveedores de forma predeterminada. 
Esto significa que es imposible inyectar proveedores que no formen parte 
directamente del módulo actual ni se exporten desde los módulos importados. 

Por lo tanto, puede considerar los proveedores exportados de un módulo como 
la interfaz pública del módulo o API.
*/
/*
- Aplicacion de middleware personalizado.
(ver archivo main.ts y luego logger.middleware.ts)

Dentro del decorador @module no existe una propiedad para los middlewares,
en su lugar, configuramos usando el metodo configure() de la clase de modulo.

Los modulos que incluyen middlewares deben implementar la interfaz NestModule.
*/
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    /* 
    Configuramos el LoggerMiddleware para los controladores
    de la ruta /products.
    
    consumer.apply(LoggerMiddleware).forRoutes('products');

    Tambien podemos restringir mas el uso del middleware especificando
    el metodo HTTP que debe tener la solicitud

    De ser necesario, el metodo configure puede llevar un async y 
    agregar un await a los middlewares que se necesiten.
    */
    consumer
      .apply(LoggerMiddleware)
      // podemos excluir rutas o metodos HTTP del middleware
      .exclude({ path: 'products', method: RequestMethod.POST })
      .forRoutes({
        path: 'products',
        method: RequestMethod.GET,
      });
    // el metodo forRoutes tambien puede recibir controladores como argumento
    // .forRoutes(ProductsController)
    /*
    Middleware funcional
    ver archivo loggerFunction.middleware.ts
    */
    consumer.apply(logger).forRoutes('/');
    /*
    Para vincular varios middlewares que se ejecuten de forma secuencial
    simplemente proporcione una lista separada por comas.
    */
    consumer.apply(cors(), helmet());
  }
}
