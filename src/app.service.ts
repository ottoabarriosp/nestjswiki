import { Injectable } from '@nestjs/common';
/*
  Los providers o proveedores son fundamentales en Nest.
  muchas de las clases basicas pueden tratarse como proveedor
  (services, repositories, factories, helpers etc)
  la idea principal de un provider es inyectar dependencias.

  esto significa que los objetos pueden crear varias relaciones entre si, y
  la funcion de conectar instancias de objetos se delega a Nest.

  un provider es simplemente una clase con el decorador @Injectable()
*/
@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
