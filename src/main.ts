import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as morgan from 'morgan';
// arranca la app
async function bootstrap() {
  /*
  NestFactory crea una instancia de la app nest
  El metodo create() devuelve un objeto de aplicación, que cumple con la INestApplicationinterfaz.
  */
  const app = await NestFactory.create(AppModule);
  /*
  Los middlewares de Nest son equivalentes a los middlewares de express.
  Para ver como implementar middlewares personalizados ver el archivo
  logger.middleware.ts y luego el app.module.ts

  para implementar middlewares de forma global, podemos hacerlo directamente
  usando el metodo use de app, exactamente igual que en express.
  */
  app.use(morgan('dev'));
  // inicia el servidor HTTP
  await app.listen(3000);
}
bootstrap();
