import Joi from '@hapi/joi';

export const ProductSchema = Joi.object({
  ean: Joi.string(),
  name: Joi.string(),
  description: Joi.string(),
});
