export interface Product {
  ean: string;
  name: string;
  description: string;
}
