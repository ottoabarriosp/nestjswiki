import { ObjectSchema } from '@hapi/joi';
import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
/*
Implementacion en products controller
@Post('validate-with-pipes')
*/
@Injectable()
export class JoiValidationPipe implements PipeTransform {
  /*
        Este Pipe recibe como argumento un schema, por lo que
        podemos utilizarlo para validar cualquier DTO o tipo de dato
        pudiendo usarse para muchos contextos diferentes.
    */
  constructor(private schema: ObjectSchema) {}

  transform(value: any, metadata: ArgumentMetadata) {
    /*
      Ejecutamos el metodo validate sobre el schema recibido
    */
    const { error } = this.schema.validate(value);
    if (error) throw new BadRequestException('Validation failed');

    return value;
  }
}
