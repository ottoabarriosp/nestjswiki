import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
/*
Los Pipes son una especie de middleware que nos ayudan a
validar o transformar los datos recibidos por parametro.

En los pipes nativos, si la operacion de transformacion falla,
se lanza una excepcion.

ParseIntPipe = convierte el string en un numero entero
https://docs.nestjs.com/pipes

PipeTransform<T,R>es una interfaz genérica que debe ser 
implementada por cualquier tubería. La interfaz genérica 
se utiliza Tpara indicar el tipo de entrada value y R para
indicar el tipo de retorno del método transform()
*/
@Injectable()
export class ValidationPipe implements PipeTransform {
  /*
    Todos los pipes deben tener el metodo transform()
    indicado en la interfaz PipeTransform, el mismo
    recibe los parametros value y metadata.

    value: es el argumento del metodo procesado actualmente
    antes de que lo reciba el controlador.

    metadata: los metadatos del argumento.
    El objeto metadata posee esta estructura
    
    type: 'body' | 'query' | 'param' | 'custom';
    metatype?: Type<unknown>;
    data?: string;
    */
  transform(value: any, metadata: ArgumentMetadata) {
    /*
      Este pipe es bastante sencillo, simplemente vamos a
      retornar el mismo valor recibido.

      Para ver otro ejemplo usando Joi ver el archivo
      joi-validation.pipe.ts
    */
    return value;
  }
}
