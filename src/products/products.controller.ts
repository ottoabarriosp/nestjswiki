import { JoiValidationPipe } from './pipes/joi-validation.pipe';
import { HttpExceptionFilter } from './filters/http-exception.filter';
import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Req,
  Res,
  UseFilters,
  UsePipes,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { CreateProductDto } from './dtos/create-product.dto';
import { Product } from './interfaces/product.interface';
import { ProductsService } from './products.service';
import { ProductSchema } from './interfaces/product.schema';

// dentro del decorador controller, podemos especificar la ruta a la cual
// atendera el controlador. por default es '/'
// @Controller('products') => /products
@Controller('products')
/* 
Filtro personalizado para manejo de excepciones,
ver archivo filters/http-exceptions.filter.ts
@UseFilters(new HttpExceptionFilter())
*/
export class ProductsController {
  // creamos una instancia de nuestro servicio
  // ver archivo app.module.ts para entender sobre providers y luego products.service.ts
  constructor(private productsService: ProductsService) {}

  // por defecto nest envia como respuesta siempre un codigo 200
  // y 201 para solicitudes POST
  // se puede cambiar este comportamiento con el decorador @HttpCode()
  @Get('set-http-code')
  @HttpCode(404)
  outOfStock(): string {
    return 'return status code 404';
  }

  // Nest emplea dos opciones diferentes para manipular las respuestas
  @Get('standard-response')
  // Estandar (return)
  findAll(): string[] {
    //cuando un controlador de solicitudes devuelve un objeto o matriz de JavaScript,
    //se serializará automáticamente en JSON.
    return ['campera', 'camisa', 'zapatos'];
  }

  @Get('express-response')
  // Especifico de express (res)
  // podemos usar el objeto de respuesta de express inyectando el
  // decorador @Res en la funcion.
  findOne(@Res() res: Response): any {
    return res.status(200).send('zapatos');
  }

  @Post('request-object')
  // Para obtener propiedades del objeto request no siempre es necesario
  // obtenerlas de forma manual, sino que podemos utilizar decoradores nativos
  // que nos facilitan esta tarea
  /* 
  @Req => req
  @Res => res
  @Next => next
  @Session => req.session
  @Param(key? :string) => req.params / req.params[key]
  @Body(key? :string) => req.body / req.body[key]
  @Query(key? :string) => req.query / req.query[key]

  se pueden crear decoradores personalizados
  https://docs.nestjs.com/custom-decorators
  */
  createOne(@Req() request: Request, @Body() product): any {
    return 'product created successfully';
  }

  // rutas parametrizadas
  @Patch('params-in-routes/:id')
  updateOne(@Param('id') id): string {
    return `update product with id ${id}`;
  }

  // Asincronismo
  // Cada función asincrónica debe devolver un objeto Promise
  @Get('async-functions')
  async findAllAsync(@Res() res): Promise<any[]> {
    return res.json([]);
  }

  // Dtos
  // Un DTO es un objeto que define cómo se enviarán los datos a través de la red.
  // ver archivo ./dtos/create-product.dto.ts
  @Post('response-with-dtos')
  // Definimos el tipo del body a recibir utilizando nuestro DTO
  async createOneUsingDto(@Body() productDto: CreateProductDto): Promise<any> {
    return 'This action adds a new product';
  }

  // los controladores siempre deben pertenecer a un modulo,
  // por eso debemos agregar el controlador al modulo de la app
  // ver archivo app.module.ts
  // ------------------------------------------------------------------------
  /* 
  El servicio ProductService se inyecta a traves del constructor de clase.
  el uso de la sintaxis "private" nos permite tanto declarar como
  inicializar el servicio de forma inmediata en la misma ubicacion.
  */
  @Post('create-using-service')
  async createUsingService(@Body() productDto: CreateProductDto) {
    this.productsService.create(productDto);
  }

  @Get('list-using-service')
  @UseFilters(new HttpExceptionFilter())
  /*
  Con el decorador UseFilters asignado a un controlador,
  ejecutamos el filtro de excepciones personalizadas para la ruta
  que atiende este controller

  ver archivo filters/http-exception.filter.ts
  */
  async findAllWithService(): Promise<Product[]> {
    return this.productsService.findAll();
  }

  @Get('convert-param/:id')
  /*
  Los Pipes son una especie de middleware que nos ayudan a
  validar o transformar los datos recibidos por parametro.

  En los pipes nativos, si la operacion de transformacion falla,
  se lanza una excepcion.

  ParseIntPipe = convierte el string en un numero entero
  https://docs.nestjs.com/pipes

  ver Pipe personalizado en
  pipes/validation.pipe.ts
  */
  getParamWithPipe(@Param('id', ParseIntPipe) id: number): string {
    return `received param with type string and convert to ${typeof id}`;
  }

  @Post('validate-with-pipes')
  //@UsePipes(new JoiValidationPipe(ProductSchema))
  validateBodyWithJoi(@Body() product: CreateProductDto): any {
    return {
      message: 'Validation successfully!!',
      data: product,
    };
  }
}
