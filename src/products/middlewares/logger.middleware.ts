import { NextFunction, Request, Response } from 'express';
import { Injectable, NestMiddleware } from '@nestjs/common';

/*
Con Nest se pueden hacer middlewares personalizados en una
funcion o clase haciendo uso del decorador @Injectable.
La clase debe implementar la interfaz NestMiddleware mientras
que la funcion no debe hacerlo.

Los middlewares de Nest son totalmente compatibles con la 
inyeccion de dependencia. Al igual que con los providers y
controladores, pueden inyectar dependencias que esten
disponibles dentro del mismo modulo a traves del constructor.
*/

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    console.log('Request...');
    next();
  }
}

// ver implementacion en app.module.ts
