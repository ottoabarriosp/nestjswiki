/*
Un DTO es un objeto que define cómo se enviarán los datos a través de la red. 

Podríamos determinar el esquema DTO usando interfaces TypeScript o clases simples. 
Curiosamente, recomendamos usar las clases aquí. 

¿Por qué? Las clases son parte del estándar JavaScript ES6 y, por lo tanto, 
se conservan como entidades reales en el JavaScript compilado. 

Por otro lado, dado que las interfaces TypeScript se eliminan durante la 
transpilación, Nest no puede hacer referencia a ellas en tiempo de ejecución.
*/

export class CreateProductDto {
  ean: string;
  name: string;
  description: string;
}
