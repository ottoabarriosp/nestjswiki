import { Injectable } from '@nestjs/common';
import { Product } from './interfaces/product.interface';
/*
  Los providers o proveedores son fundamentales en Nest.
  muchas de las clases basicas pueden tratarse como proveedor
  (services, repositories, factories, helpers etc)
  la idea principal de un provider es inyectar dependencias.

  Al igual que los controllers, estos deben pertenecer a un modulo.
  ver archivo app.module.ts
*/

// este servicio esta creado para ser utilizado por el ProductsController
// por lo que lo definimos como provider agregando el decorador @Injectable()
@Injectable()
// El decorador @Injectable adjunta metadatos a la clase,
// lo que le dice a Nest que esta clase es un proveedor de Nest.
export class ProductsService {
  private readonly products: Product[] = [];
  // usamos una interface para definir el tipado de nuestros metodos
  // ver archivo product.interface.ts
  create(product: Product) {
    this.products.push(product);
  }

  findAll(): Product[] {
    return this.products;
  }
}
