/*
Nest viene con una capa de excepciones incorporada que es responsable de
procesar todas las excepciones no controladas en una aplicación. 
Cuando el código de su aplicación no maneja una excepción, esta capa la detecta, 
que luego envía automáticamente una respuesta adecuada y fácil de usar.

Si bien el filtro de excepciones incorporado funciona bastante bien, es probable
que queramos enviar una respuesta personalizada cuando no logremos
manejar una excepcion, los filtros de excepcion estan diseñados exactamente
con este proposito.
*/

import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';

/*
El decorador @Catch vincula los metadatos requeridos al filtro de excepciones
y le dice a Nest que este filtro en particular busca excepciones
del tipo indicado, en este caso de tipo HttpException.

Tambien puede recibir una lista separada por comas con los tipos de excepciones
a capturar por el filtro.

Si dejamos el Catch() sin ningun argumento, atrapara
todas las excepciones sin importar el tipo.
*/
@Catch(HttpException)
// extendemos la clase del filtro nativo de nest.
export class HttpExceptionFilter implements ExceptionFilter {
  /*
  El parametro exception es el objeto de excepcion que se esta presentando actualmente
  El parametro host es un objeto de tipo ArgumentsHost y lo usamos para obtener
  una referencia a los objetos Request y Response que recibe el controlador de la
  solicitud original. (donde se origina la excepcion)
  */
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
