/*
El controlador y el servicio de productos pertenecen
al mismo dominio de aplicacion, al estar tan relacionados
tiene sentido encapsularlos dentro de un mismo modulo.

Esto nos permite organizar nuestra aplicacion estableciendo
limites claros entre las diferentes funciones y tener una
arquitectura facilmente escalable.
*/
import { Global, Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';

/*
Si se tiene un conjunto de modulos o funcionalidades que se importan
en muchos sitios de la aplicacion o se desee proporcionar un conjunto
de providers que deben estar disponibles en todas partes desde el primer momento
(helpers, conexiones a la DB, etc) puede hacer un modulo global agregnado
el decorador @Global()
*/
@Global()
/*
De esta forma, el modulo tendra un scope global.
Los modulos globales deben registrarse una sola vez, generalmente por el modulo
raiz o principal.

en este ejemplo, los modulos que deseen inyectar el servicio no necesitan 
importar el modulo Products en su array de imports.

* hacer muchos modulos globales no es una buena decision de diseno, los modulos
globales estan disponibles para reducir la cantidad de repeticion innecesaria.
*/
@Module({
  controllers: [ProductsController],
  providers: [ProductsService],
  /*
  - Módulos compartidos
  En Nest, los modulos son singletons de forma predeterminada y por tanto,
  puede compartir la misma instancia de cualquier provider entre varios
  modulos.
  (https://en.wikipedia.org/wiki/Singleton_pattern)
  
  Cada modulo es automaticamente un modulo compartido, una vez creado
  cualquier modulo puede reutilizarlo.

  si queremos compartir una instancia de nuestro servicio entre varios
  modulos, debemos exportarlo agregandolo al array de exports.
  */
  exports: [ProductsService],
  /*
  Ahora cualquier modulo que importe que modulo ProductsModule va a tener acceso
  al servicio de products y compartira la misma instancia que todos los demas modulos
  que tambien lo importen.
  (si es global, no hace falta importarlo)

  - Módulo de reexportación
  Puedes importar varios modulos y exportarlos desde el mismo modulo, de esta forma
  al importar un solo modulo se tienen varias funcionalidades encapsuladas.
  */
})
export class ProductsModule {
  /* 
  - Inyeccion de dependencia
    Una clase de módulo también puede inyectar proveedores
    (por ejemplo, para fines de configuración)

    Sin embargo, las clases de módulos en sí mismas no pueden inyectarse 
    como proveedores debido a la dependencia circular.
  */
  //constructor(private productsService: ProductsService) {}
}

/*
Una vez definido nuestro modulo debemos hacer la importacion
en el modulo raiz de nuestra aplicacion
ver archivo app.module.ts
*/
