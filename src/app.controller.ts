import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

// Este decorador especifica que esta clase se va a
// comportar como un controlador.
/*
Los decoradores asocian las clases con los metadatos
requeridos y permiten que Nest cree un mapa de enrutamiento
(vincula las solicitudes a los controladores correspondientes).
*/
// dentro del decorador controller, podemos especificar la ruta a la cual
// atendera el controlador. por default es '/'
// @Controller() => '/'
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  // se define el metodo HTTP que va a atender esta funcion
  // y entre () la ruta. por default es '/'
  // @Get('hello') => /hello
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
